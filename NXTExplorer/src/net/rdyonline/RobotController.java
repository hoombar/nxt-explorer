package net.rdyonline;

import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.robotics.RegulatedMotor;

public class RobotController {

	/***
	 * How fast to spin the motors
	 */
	private final int 							MOTOR_SPEED			= 200;
	/***
	 * This is to account for the distance between the wheels. 
	 * The further apart, the greater the value needed
	 */
	private final float							BODY_ROTATION_DIST	= 2.3f;
	/***
	 * How far you travel for a full rotation of one of the motors in CM
	 */
	private final float							ROTATION_DISTANCE	= 17;
	
	private static volatile RobotController		instance			= null;
	
	// Use these definitions instead if your motors are inverted
	// 		static RegulatedMotor leftMotor = MirrorMotor.invertMotor(Motor.C);
	//		static RegulatedMotor rightMotor = MirrorMotor.invertMotor(Motor.B);
	private final RegulatedMotor				headMotor			= Motor.A;
	private final RegulatedMotor 				leftMotor 			= Motor.C;
	private final RegulatedMotor 				rightMotor 			= Motor.B;
	
	public enum direction {
		left,
		right
	}
	
	public enum anatomy {
		head,
		body
	}
	
	private RobotController() {
		headMotor.setSpeed(MOTOR_SPEED);
		
	    leftMotor.setSpeed(MOTOR_SPEED);
	    rightMotor.setSpeed(MOTOR_SPEED);
	}
	
	public static synchronized RobotController getInstance() {
		if (instance == null) {
			instance 		= new RobotController();
		}
		
		return instance;
	}
	
	public void turn(anatomy appendage, direction dir, int degrees) {
		switch (appendage) {
			case body:
				turnBody(dir, degrees);
				break;
			case head:
				turnHead(dir, degrees);
				break;
		}
	}
	
	public void forward() {
		leftMotor.forward();
		rightMotor.forward();
	}
	
	public void forward(int distance) {
		
		float 	unit		= 360/ROTATION_DISTANCE;

		distance			= (int) (distance * unit);
		
		leftMotor.rotate(distance, true);
		rightMotor.rotate(distance);
		stop();
	}
	
	public void back() {
		leftMotor.backward();
		rightMotor.backward();
	}
	
	public void back(int distance) {
		float 	unit		= 360/ROTATION_DISTANCE;
		
		distance			= (int) (distance * unit);
		distance			*= -1;
		
		// need to be a bit more careful going back because of being front heavy and not loosing balance
	    leftMotor.setSpeed(MOTOR_SPEED/2);
	    rightMotor.setSpeed(MOTOR_SPEED/2);
		
		leftMotor.rotate(distance, true);
		rightMotor.rotate(distance);
		
	    leftMotor.setSpeed(MOTOR_SPEED);
	    rightMotor.setSpeed(MOTOR_SPEED);
	    
		stop();
	}
	
	public void stop() {
		leftMotor.stop(true);
		rightMotor.stop(true);
		headMotor.stop(true);
	}
	
	private void turnBody(direction dir, int degrees) {
		int 	leftVal 		= 0;
		int 	rightVal 		= 0;
		
		degrees 				*= BODY_ROTATION_DIST;
		
		switch (dir) {
			case left:
				leftVal			= degrees * -1;
				rightVal		= degrees;
				break;
			case right:
				leftVal			= degrees;
				rightVal		= degrees * -1;
				break;
		}
		
		leftMotor.rotate(leftVal, true);
		rightMotor.rotate(rightVal);
		stop();
	}
	
	private void turnHead(direction dir, int degrees) {
		switch (dir) {
			case left:
				degrees			= degrees * -1;
				break;
		}
		
		headMotor.rotate(degrees);
		stop();
	}
	
	public String getDistanceTravelled() {
		
		String result			= "";
		
		double	distance		= leftMotor.getTachoCount() * 17;
		distance 				/= 360;
		
		String 	unit			= "CM";
		
		// distance is in cm
		if (distance > 100000) {
			unit				= "KM";
			distance			/= 100000;
		} else if (distance > 100) {
			unit				= "M";
			distance			/= 100;
		}
		
		String 	sDistance		= Double.toString(distance);
		int		dp				= 0;
		for (char c : sDistance.toCharArray()) {
			result				+= c;
			
			if (dp == 2) break;
			
			if (dp > 0) dp++;
			if (c == '.') {
				dp++;
			}
		}
		
		return result + " " + unit;
	}
	
	// TODO(benp) re-do dance method
	public void dance() {
		leftMotor.rotate(-800, true);
		rightMotor.rotate(800, true);
		
		headMotor.rotate(-15);
		headMotor.rotate(30);
		headMotor.rotate(-15);
		
		headMotor.rotate(-15);
		headMotor.rotate(30);
		headMotor.rotate(-15);

		headMotor.rotate(-15);
		headMotor.rotate(30);
		headMotor.rotate(-15);
		
		sleepIndeterminate(400);
		
		leftMotor.rotate(-90, true);
		headMotor.rotate(90);
		
		sleepIndeterminate(90);
		
		leftMotor.rotate(90, true);
		headMotor.rotate(-90);
		
		sleepIndeterminate(90);
		
		rightMotor.rotate(-90, true);
		headMotor.rotate(-90);
		
		sleepIndeterminate(90);
		
		rightMotor.rotate(90, true);
		headMotor.rotate(90);
		
		sleepIndeterminate(90);
	}
	
	private void sleepIndeterminate(float sleepValue) {
		try {
			float 	sleep 		= ((float) sleepValue / MOTOR_SPEED) * 1000;
			
			Thread.sleep((long) sleep);
		} catch (InterruptedException e) {
			System.out.println(e.toString());
			Button.waitForAnyPress();
		}
	}
}
