package net.rdyonline.behaviors;

import lejos.nxt.Button;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import lejos.nxt.UltrasonicSensor;
import lejos.robotics.subsumption.Behavior;
import net.rdyonline.RobotController;
import net.rdyonline.RobotController.anatomy;
import net.rdyonline.RobotController.direction;

public class DetectWall implements Behavior {

	// there is a slight delay before it registers when moving forwards because of the bluetooth
	private final int			SONAR_WALL_DISTANCE		= 35;
	
	private TouchSensor 		touch					= null;
	private UltrasonicSensor 	sonar					= null;
	private RobotController		control					= RobotController.getInstance();
	
	private boolean 			touchPressed			= false;
	
	private direction			nextDirection			= direction.left;
	
	public DetectWall() {
		touch 		= new TouchSensor(SensorPort.S1);
		sonar 		= new UltrasonicSensor(SensorPort.S4);
	}

	/***
	 * Once we have bumped in to something or the sonar detects it's less than 25 CM from
	 * an object, move away from the wall/block
	 */
	public boolean takeControl() {
		// get a reading from the sonar sensor
		sonar.ping();
    
		touchPressed		= touch.isPressed();
		
		return touchPressed || sonar.getDistance() < SONAR_WALL_DISTANCE;
	}

	/***
	 * Since  this is highest priority behaviour, suppress will never be called.
	 */
	public void suppress() {
		// nothing to do, see method header
	}

	/***
	 * The action that is taken when the wall is hit
	 */
	public void action() {
		
		int 		angle		= 0;

		control.stop();

		/*
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 */
		
		// back up a little to we don't get wedged
		control.back(5);
		
		if (touchPressed) {
			touchPressed		= false;
			
			control.turn(anatomy.body, direction.left, 180);
		}
		
		while (angle == 0) {
			// work out what direction to move in
			angle				= getDirectionToMove();
			if (angle == 0) {
				control.turn(anatomy.body, direction.left, 180);
				continue;
			}
			
			direction	dir		= direction.right;
			
			if (angle < 0) {
				dir				= direction.left;
				// change -30 in to 30 as that's what the turn will expect
				angle			*= -1; 
			}
			
			// turn to the right
			control.turn(anatomy.body, dir, angle);
		}
	}
	
	/***
	 * Scan around to see where the most space is
	 * @return angle to move to (up to 45 degrees each way). from -45 to 45
	 */
	private int getDirectionToMove() {
		// TODO(benp) alternate between scan right and scan left to find open space
		
		int 		result		= 0;
		int			maxValue	= 0;
		direction	dir			= direction.left;
		
		if (nextDirection == direction.right) {
			dir					= direction.right;
			
			nextDirection		= direction.left;
		} else {
			nextDirection 		= direction.right;
		}
		
		if (dir == direction.right) {
			result 					= getDirectionScanRight(dir, maxValue, result);	
		} else {
			result 					= getDirectionScanLeft(dir, maxValue, result);
		}
		
		
		if (result == 0) {
			
			// scanning the other direction now
			if (dir == direction.right) {
				result 					= getDirectionScanLeft(dir, maxValue, result);	
			} else {
				result 					= getDirectionScanRight(dir, maxValue, result);
			}
			
		}
		
		if (dir == direction.left) {
			result				*= -1;
		}
		
		/*
		if (maxValue < SONAR_WALL_DISTANCE) {
			// by returning 0, the robot will do a 180, so need to invert the next direction
			if (nextDirection == direction.left) {
				nextDirection = direction.right;
			} else {
				nextDirection = direction.left;
			}
			
			result = 0;
		}
		*/
		
		return result;
	}
	
	private int getDirectionScanLeft(direction dir, int maxValue, int result) {
		for (int i = 0; i < 135; i+=15) {
			control.turn(anatomy.head, direction.left, 15);
			sonar.ping();
			int 	measure		= sonar.getDistance();
			
			if (measure > maxValue) {
				maxValue 		= measure;
				result			= i;
			}
			
			// should be the last point at which it measures 255
			if (measure == 255) {
				result 		= i;
			}
		}
		control.turn(anatomy.head, direction.right, 135);
		
		return result;
	}
	
	private int getDirectionScanRight(direction dir, int maxValue, int result) {
		for (int i = 0; i < 135; i+=15) {
			control.turn(anatomy.head, direction.right, 15);
			sonar.ping();
			int 	measure		= sonar.getDistance();
			
			if (measure > maxValue) {
				dir				= direction.right;
				maxValue 		= measure;
				result			= i;
			}
			
			
			if (measure == 255) {
				result 		= i;
			}
		}
		control.turn(anatomy.head, direction.left, 135);
		
		return result;
	}
}
