package net.rdyonline.behaviors;

import lejos.nxt.LCD;
import lejos.robotics.subsumption.Behavior;
import net.rdyonline.RobotController;

public class DriveForward implements Behavior {

	private boolean 			suppressed 		= false;
	private RobotController		control			= RobotController.getInstance();

	/***
	 * This behaviour always wants control 
	 */
	public boolean takeControl() {
		return true;
	}

	/***
	 * Standard action for a suppress method
	 */
	public void suppress() {
		suppressed = true;
	}

	/***
	 * The action to perform when we should be driving forward
	 */
	public void action() {
		suppressed = false;
	    
		control.forward();
	    
	    while (!suppressed) {
	    	// update display with the distance
	    	
	    	//TODO(benp) update the display with the distance (uncomment this line)
	    	//updateDisplay();
	    	
	    	//don't exit till suppressed
	    	Thread.yield(); 
	    }
	    
	    control.stop();
	}
	
	private void updateDisplay() {
		LCD.clear();
		LCD.drawString("Dist: " + control.getDistanceTravelled(), 1, 1);
		LCD.refresh();
	}
}