package net.rdyonline.behaviors;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.microedition.io.Connection;

import lejos.nxt.LCD;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;
import lejos.robotics.subsumption.Behavior;

public class ListenForMessage implements Behavior {

	private final int 			STATE_UNDETERMINED		= -1;
	private final int 			STATE_STOP				= 1;
	private final int 			STATE_YIELD				= 2; 
	
	private boolean 			suppressed 				= false;
	private NXTConnection		bluetoothConn			= null;
	
	private int 				state					= STATE_UNDETERMINED;

	public ListenForMessage(NXTConnection connection) {
		bluetoothConn 		= connection;
	}
	
	/***
	 * This behaviour always wants control 
	 */
	public boolean takeControl() {

		boolean 	result 			= false;
		int 		code 			= readValue();
        
		switch (code) {
			case STATE_UNDETERMINED:
				result 				= false;
				break;
			case STATE_STOP:
				result 				= true;
				break;
			case STATE_YIELD:
				result 				= false;
				break;
		}
        
		state 		= code;
		
		return result;
	}

	/***
	 * Standard action for a suppress method
	 */
	public void suppress() {
		suppressed = true;
	}

	/***
	 * The action to perform when we should be driving forward
	 */
	public void action() {
		// nothing to do here. Simply wait until control is released
		while (!suppressed) {
			Thread.yield();
		}
	}
	
	private int readValue() {
		
		int 				n			= -1;
		DataInputStream 	dis			= null;
		
    	try {
    		dis							= bluetoothConn.openDataInputStream();
    		
    		byte[] buf = new byte[1];
    		LCD.clear();
    		LCD.drawString("before read", 1, 1);
    		LCD.refresh();
    		
    		Thread.sleep(1000);
    		
    		n = dis.read(buf);
    		//n 							= dis.readByte();
            
    		LCD.clear();
    		LCD.drawString("after read" + n, 1, 1);
    		LCD.refresh();
    		
            dis.close();
            
            
            Thread.sleep(1000);
            
		} catch (Exception e) {
			LCD.clear();
		    LCD.drawString("no conn", 4, 4);
			LCD.refresh();
			
			n 		= state;
		} 

    	return n;
	}

}