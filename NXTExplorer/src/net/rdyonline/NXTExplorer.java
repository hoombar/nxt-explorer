package net.rdyonline;

import lejos.nxt.LCD;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;
import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;
import net.rdyonline.behaviors.DetectWall;
import net.rdyonline.behaviors.DriveForward;
import net.rdyonline.behaviors.ListenForMessage;

/***
 * Main entry point for the NXT Explorer program
 * 
 * Start with a dance and then begin exploring :)
 * 
 * @author rdy
 *
 */
public class NXTExplorer {
	
	private static 			Arbitrator			arbitrator			= null;
	private static			RobotController		control				= RobotController.getInstance();
	private static 			NXTConnection		bluetoothConn		= null;
  
	public static void main(String[] args) {
		
		bluetoothConn 		= Bluetooth.waitForConnection();

		bluetoothConn.setIOMode(NXTConnection.RAW);

		setupArbitrator();
		
	    // done showing off, let's get started
	    startExploring();
	}
	
	private static void setupArbitrator() {
		// chain together the behaviours
		Behavior 		driveForward 		= new DriveForward();
	    Behavior 		detectWall 			= new DetectWall();
	    Behavior 		listenForMessage	= new ListenForMessage(bluetoothConn);
	    
	    //Behavior[] 		behaviorList 		= { listenForMessage };
	    Behavior[] 		behaviorList 		= { driveForward, detectWall, listenForMessage };
	    
	    arbitrator 							= new Arbitrator(behaviorList);
	}
	
	private static void startExploring() {
	    // start exploring (run behaviours)
	    arbitrator.start();
	}
}

